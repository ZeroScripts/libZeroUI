print("Loading library...")
local require = loadstring(game:HttpGet("https://zeroscripts.gitlab.io/libraries/librequire.lua"))()
function dump(o)
   if type(o) == 'table' then
      local s = '{ '
      for k,v in pairs(o) do
         if type(k) ~= 'number' then k = '"'..k..'"' end
         s = s .. '['..k..'] = ' .. dump(v) .. ','
      end
      return s .. '} '
   else
      return tostring(o)
   end
end
local libZeroUI = require("libzeroui")
print(dump(libZeroUI))
print("Unit test for libZeroUI synchronous functions...")
print("Creating window...")
local wnd = libZeroUI.CreateWindow("Love you Kurumi <3")
for k, v in pairs(wnd) do
    print(k, v)
end
print("Creating Tab1...")
local tab1 = wnd.Tabs:CreateTab("Xx_Kurumi_xX").First
tab1:AddLabel("Kurumi")
local tab1btn1 = tab1:AddButton("I love Kurumi!")
print(tab1btn1:GetText())
tab1btn1:AddCallback(function()
    print("I love Kurumi!")
end)
print("Creating Tab2...")
local tab2 = wnd.Tabs:CreateTab("Aleph", 2)
for _ = 1, 50, 1 do
    tab2.First:AddLabel("Kurumi refrain")
    tab2.First:AddTextbox("Great LN volume")
    tab2.First:AddButton("Yes!", function() 
        print("Of course you will love Kurumi arc.")
    end)
    tab2.Last:AddLabel("Kurumi raganok")
    tab2.Last:AddTextbox("Another great LN volume", "Yep")
    tab2.Last:AddButton("Yes!", function() 
        print("Volume 18 according to spoilers Mio will bonk you.")
    end)
end
local tab3 = wnd.Tabs:CreateTab("Bet", 3)
tab3.First:AddLabel("Kurumi 1")
tab3.Frames[1]:AddLabel("Kurumi 2")
tab3.Frames[1]:AddLabel("This is a demonstration of a tab with")
tab3.Frames[1]:AddLabel("multiple frames.")
tab3.Last:AddLabel("Kurumi 3")
print("Showing window...")
wnd:ShowWindow()
print("Hiding window...")
wnd:HideWindow()
print("Showing window...")
wnd:ShowWindow()
print("Renaming window...")
wnd:SetTitle("Still, love you Kurumi <3")
-- wait(1)
-- print("Closing window...")
-- wnd:CloseWindow()
print("Unit test for libZeroUI completed.")
