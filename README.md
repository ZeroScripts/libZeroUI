[![pipeline status](https://gitlab.com/ZeroScripts/libZeroUI/badges/master/pipeline.svg)](https://gitlab.com/ZeroScripts/libZeroUI/-/commits/master)
[![coverage report](https://gitlab.com/ZeroScripts/libZeroUI/badges/master/coverage.svg)](https://gitlab.com/ZeroScripts/libZeroUI/-/commits/master)
# libZeroUI

> A simple User Interface for users and developers to use

## TODOs:
- [x] Rojo Project Structure
- [x] Base library working (libZeroUI itself)
- [ ] Building support
- [ ] GitLab CI
- [ ] Rewrite the core script
- [ ] Add more features

### License
See LICENSE
