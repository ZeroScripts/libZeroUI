-- Roblox things
local RunService = game:GetService("RunService")

local Window

if RunService:IsStudio() then
    local dependency = script.Parent
    Window = require(dependency.Objects.Window)
    warn("Roblox Studio detected, the result may be different from normal game environments.")
else
    local require
    xpcall(function() 
        if not getgenv().ZeroScripts then
            error("") -- Dummy so it can execute xpcall error handling.
        end
        if getgenv().ZeroScripts["librequire"] then
            require = getgenv().ZeroScripts.librequire
        end
        if getgenv().ZeroScripts["libZeroUI"] then
            require = getgenv().ZeroScripts.libZeroUI.require
        end
        if not require then
            error("")
        end
    end, function(_)
        require = loadstring(game:HttpGet("https://zeroscripts.gitlab.io/libraries/librequire.lua"))()
    end)
    getgenv().ZeroScripts.libZeroUI = {
        require = require,
        Utilities = require("libzeroui/utilities"),
        Constants = require("libzeroui/constants")
    }
    Window = require("libzeroui/objects/window")
end

local library = {}
function library.CreateWindow(title, size, icon)
    if not size then
        size = { 
            x = 960,
            y = 540 
        }
    end
    return Window.new(title, size, icon)
end
return library
