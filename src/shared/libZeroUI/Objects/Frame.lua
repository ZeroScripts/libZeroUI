local RunService = game:GetService("RunService")

local Utilities, Constants
local Textbox, Label, Button

if RunService:IsStudio() then
    local dependency = script.Parent
    Utilities = require(dependency.Parent.Utilities)
    Constants = require(dependency.Parent.Constants)

    Textbox = require(dependency.Textbox)
    Label = require(dependency.Label)
    Button = require(dependency.Button)
else
    local require = getgenv().ZeroScripts.libZeroUI.require
    Utilities = getgenv().ZeroScripts.libZeroUI.Utilities
    Constants = getgenv().ZeroScripts.libZeroUI.Constants

    Textbox = require("libzeroui/objects/textbox")
    Label = require("libzeroui/objects/label")
    Button = require("libzeroui/objects/button")
end

local Frame = {}
Frame.__index = Frame

function Frame.new(container)
    local frame = {
        _objects = {},

    }
    frame._objects.frame = Utilities:CreateObject("ScrollingFrame", {
        Parent = container,
        BackgroundColor3 = Constants.DARK35,
        BorderSizePixel = 0,
        ScrollBarThickness = 4,
        ScrollingDirection = Enum.ScrollingDirection.Y,
        CanvasSize = UDim2.new(0, 0, 0, 0),
        ScrollBarImageColor3 = Constants.GRAY100
    })
    Utilities:CreateObject("UIListLayout", {
        Parent = frame._objects.frame,
        FillDirection = Enum.FillDirection.Vertical,
        HorizontalAlignment = Enum.HorizontalAlignment.Center,
        SortOrder = Enum.SortOrder.LayoutOrder
    })
    setmetatable(frame, Frame)
    frame:AddWhitespace()
    return frame
end

function Frame:_AddCanvasHeight(height)
    self._objects.frame.CanvasSize = UDim2.new(0, 0, 0, self._objects.frame.CanvasSize.Y.Offset + height)
end

function Frame:AddWhitespace(length)
    if not length then
        length = 5
    end
    local index = #self._objects
    local object = Utilities:CreateObject("Frame", {
        Parent = self._objects.frame,
        BackgroundTransparency = 1,
        BorderSizePixel = 0,
        LayoutOrder = index,
        Size = UDim2.new(1, -10, 0, 5),
    })
    self._objects[index] = object
    self:_AddCanvasHeight(5)
    return object
end

function Frame:AddLabel(text, no_whitespace)
    local index = #self._objects
    local object = Label.new(text, self._objects.frame, index)
    if not no_whitespace then
        self:AddWhitespace()
    end
    self:_AddCanvasHeight(object:GetObjectHeight())
    self._objects[index] = object
    return object
end

function Frame:AddTextbox(placeholder_text, text, no_whitespace)
    local index = #self._objects
    local object = Textbox.new(placeholder_text, text, self._objects.frame, index)
    if not no_whitespace then
        self:AddWhitespace()
    end
    self:_AddCanvasHeight(object:GetObjectHeight())
    self._objects[index] = object
    return object
end

function Frame:AddButton(text, callback, no_whitespace)
    local index = #self._objects
    local object = Button.new(text, callback, self._objects.frame, index)
    if not no_whitespace then
        self:AddWhitespace()
    end
    self:_AddCanvasHeight(object:GetObjectHeight())
    self._objects[index] = object
    return object
end

return Frame