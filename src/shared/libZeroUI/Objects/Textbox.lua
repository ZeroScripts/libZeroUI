local RunService = game:GetService("RunService")

local Utilities, Properties

if RunService:IsStudio() then
    local dependency = script.Parent
    Utilities = require(dependency.Parent.Utilities)
    local properties = script.Parent.Parent.Properties
    Properties = require(properties.Textbox)
else
    local require = getgenv().ZeroScripts.libZeroUI.require
    Utilities = getgenv().ZeroScripts.libZeroUI.Utilities
    Properties = require("libzeroui/properties/textbox")
end

local Textbox = {}
Textbox.__index = Textbox

function Textbox.new(placeholder_text, text, frame, index)
    if not text then
        text = ""
    end
    local textbox = {
        _objects = {}
    }
    textbox._objects.frame = Utilities:CreateObject("Frame", Properties.Frame, {
        Parent = frame,
        LayoutOrder = index
    })
    textbox._objects.textbox = Utilities:CreateObject("TextBox", Properties.TextBox, {
        Parent = textbox._objects.frame,
        Text = text,
        PlaceholderText = placeholder_text
    })
    setmetatable(textbox, Textbox)
    return textbox
end

function Textbox:GetText()
    return self._objects.textbox.Text
end

function Textbox:SetText(text)
    self._objects.textbox.Text = text
end

function Textbox:GetPlaceholderText()
    return self._objects.textbox.PlaceholderText
end

function Textbox:SetPlaceholderText(text)
    self._objects.textbox.PlaceholderText = text
end

function Textbox:GetObjectHeight()
    return self._objects.frame.Size.Y.Offset
end

function Textbox:GetObject()
    return self._objects.textbox
end

return Textbox