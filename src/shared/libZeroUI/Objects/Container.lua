local RunService = game:GetService("RunService")

local Utilities, Frame

if RunService:IsStudio() then
    local dependency = script.Parent
    Utilities = require(dependency.Parent.Utilities)
    Frame = require(dependency.Frame)
else
    local require = getgenv().ZeroScripts.libZeroUI.require
    Utilities = getgenv().ZeroScripts.libZeroUI.Utilities
    Frame = require("libzeroui/objects/frame")
end

local Container = {}
Container.__index = Container

function Container.new(tab, frameCount)
    local container = {
        Frames = {}
    }
    local frameSize = 1 / frameCount
    Utilities:CreateObject("UIGridLayout", {
        Parent = tab.tabFrame,
        SortOrder = Enum.SortOrder.LayoutOrder,
        CellSize = UDim2.new(frameSize, -5, 1, -5),
        CellPadding = UDim2.new(0, 5, 0, 5),
        HorizontalAlignment = Enum.HorizontalAlignment.Center,
    })
    for i = 0, frameCount - 1, 1 do 
        local frame = Frame.new(tab.tabFrame)
        if i == 0 then
            container.First = frame
        elseif i == frameCount - 1 then
            container.Last = frame
        end
        container.Frames[i] = frame
    end
    setmetatable(container, Container)
    -- TODO
    return container
end

return Container