local RunService = game:GetService("RunService")

local Utilities, Properties

if RunService:IsStudio() then
    local dependency = script.Parent
    Utilities = require(dependency.Parent.Utilities)
    local properties = script.Parent.Parent.Properties
    Properties = require(properties.Button)
else
    local require = getgenv().ZeroScripts.libZeroUI.require
    Utilities = getgenv().ZeroScripts.libZeroUI.Utilities
    Properties = require("libzeroui/properties/button")
end

local Button = {}
Button.__index = Button

function Button.new(text, callback, frame, index)
    if not text then
        text = ""
    end
    local button = {
        _objects = {},
        _callbacks = {}
    }
    button._objects.textButton = Utilities:CreateObject("TextButton", Properties, {
        Parent = frame,
        LayoutOrder = index,
        Text = text,
    })
    button._objects.textButton.MouseButton1Click:Connect(function()
        button:FireCallbacks()
    end)
    if callback then
        button._callbacks[#button._callbacks] = callback
    end
    setmetatable(button, Button)
    return button
end

function Button:GetText()
    return self._objects.textButton.Text
end

function Button:GetCallbacks()
    return self._callbacks
end

function Button:GetCallback(index)
    return self._callbacks[index]
end

function Button:SetText(text)
    self._objects.textButton.Text = text
end

function Button:AddCallback(callback)
    local index = #self._callbacks
    self._callbacks[index] = callback
    return index
end

function Button:RemoveCallbacks()
    -- We should've repopulated the table, it's better than this.
    self._callbacks = {}
end


function Button:RemoveCallback(index)
    -- We should've repopulated the table, it's better than this.
    self._callbacks[index] = nil
end

function Button:FireCallback(index)
    local callback = self._callbacks[index]
    if callback then
        callback()
    else
        error("Callback index " .. index .. " does not exist.")
    end
end

function Button:FireCallbacks()
    for i = 0, #self._callbacks do
        local v = self._callbacks[i]
        if v then
            v()
        end
    end
end

function Button:GetObjectHeight()
    return self._objects.textButton.Size.Y.Offset
end

function Button:GetObject()
    return self._objects.textButton
end

return Button