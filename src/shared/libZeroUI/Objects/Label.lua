local RunService = game:GetService("RunService")

local Utilities, Properties

if RunService:IsStudio() then
    local dependency = script.Parent
    Utilities = require(dependency.Parent.Utilities)
    local properties = script.Parent.Parent.Properties
    Properties = require(properties.Label)
else
    local require = getgenv().ZeroScripts.libZeroUI.require
    Utilities = getgenv().ZeroScripts.libZeroUI.Utilities
    Properties = require("libzeroui/properties/label")
end

local Label = {}
Label.__index = Label

function Label.new(text, frame, index)
    if not text then
        text = ""
    end
    local label = {
        _objects = {}
    }
    label._objects.textlabel = Utilities:CreateObject("TextLabel", Properties, {
        Parent = frame,
        LayoutOrder = index,
        Text = text,
    })
    setmetatable(label, Label)
    return label
end

function Label:GetText()
    return self._objects.textlabel.Text
end

function Label:SetText(text)
    self._objects.textlabel.Text = text
end

function Label:GetObjectHeight()
    return self._objects.textlabel.Size.Y.Offset
end

function Label:GetObject()
    return self._objects.textlabel
end

return Label