-- Roblox things
local RunService = game:GetService("RunService")

local Utilities, Constants, Container

if RunService:IsStudio() then
    local dependency = script.Parent
    Utilities = require(dependency.Parent.Utilities)
    Constants = require(dependency.Parent.Constants)
    Container = require(dependency.Container)
else
    local require = getgenv().ZeroScripts.libZeroUI.require
    Utilities = getgenv().ZeroScripts.libZeroUI.Utilities
    Constants = getgenv().ZeroScripts.libZeroUI.Constants
    Container = require("libzeroui/objects/container")
end

local Tabs = {}
Tabs.__index = Tabs

function Tabs.new(window)
    local tabs = {
        _objects = {},
        _tabs = {},
        _elements = {},
        _activeTab = nil
    }
    -- TODO: Switch to ScrollingFrame
    local selectFrame = Utilities:CreateObject("Frame", {
        Parent = window,
        BackgroundColor3 = Constants.DARK40,
        BorderSizePixel = 0,
        Position = UDim2.new(0, 0, 0, 30),
        Size = UDim2.new(1, 0, 0, 40),
    })
    tabs._objects.selectFrame = selectFrame
    Utilities:CreateObject("UIListLayout", {
        Parent = selectFrame,
        FillDirection = Enum.FillDirection.Horizontal,
        SortOrder = Enum.SortOrder.LayoutOrder
    })
    local baseFrame = Utilities:CreateObject("Frame", {
        Parent = window,
        BackgroundColor3 = Constants.DARK40,
        BorderSizePixel = 0,
        Position = UDim2.new(0, 0, 0, 70),
        Size = UDim2.new(1, 0, 1, -70),
    })
    tabs._objects.baseFrame = baseFrame
    setmetatable(tabs, Tabs)
    return tabs
end

function Tabs:SetActiveTab(tabName)
    local tab = self._tabs[tabName]
    if not tab then
        error("Invalid tab: " .. tab)
        return
    end
    if tab == self._activeTab then
        return
    end
    if self._activeTab then
        local tabBtn = self._activeTab.tabBtn
        Utilities.SetProperties(tabBtn, {
            BackgroundColor3 = Constants.DARK40,
            AutoButtonColor = true
        })
        self._activeTab.activeBorderFrame.Visible = false
        self._activeTab.tabFrame.Visible = false
    end
    local tabBtn = tab.tabBtn
    Utilities.SetProperties(tabBtn, {
        BackgroundColor3 = Constants.DARK30,
        AutoButtonColor = false
    })
    tab.activeBorderFrame.Visible = true
    tab.tabFrame.Visible = true
    self._activeTab = tab
end

function Tabs:CreateTab(tabName, windowCount)
    -- Make windowCount optional argument.
    if not windowCount then
        windowCount = 1
    end
    local order = #self._elements
    self._tabs[tabName] = {}
    table.insert(self._elements, tabName)
    local tab = self._tabs[tabName]
    if order > 0 then
        local tabSeperator = Utilities:CreateObject("Frame", {
            Parent = self._objects.selectFrame,
            BackgroundColor3 = Constants.DARK60,
            BorderSizePixel = 0,
            LayoutOrder = order,
            Size = UDim2.new(0, 1, 1, -2)
        })
        tab.tabSeperator = tabSeperator
        table.insert(self._elements, tabSeperator)
        order += 1
    end
    local tabBtn = Utilities:CreateObject("TextButton", {
        Parent = self._objects.selectFrame,
        BackgroundColor3 = Constants.DARK40,
        BorderSizePixel = 0,
        Text = tabName,
        Font = Enum.Font.Gotham,
        TextSize = Constants.FONT_SIZE,
        TextColor3 = Constants.WHITE,
        TextXAlignment = Enum.TextXAlignment.Center,
        TextYAlignment = Enum.TextYAlignment.Center,
        LayoutOrder = order
    })
    tabBtn.Size = UDim2.new(0, tabBtn.TextBounds.X + 30, 1, 0)
    tab.tabBtn = tabBtn
    local activeBorderFrame = Utilities:CreateObject("Frame", {
        Parent = tabBtn,
        BackgroundColor3 = Constants.BLUE,
        BorderSizePixel = 0,
        Position = UDim2.new(0, 0, 1, -2),
        Size = UDim2.new(1, 0, 0, 2),
        Visible = false
    })
    tab.activeBorderFrame = activeBorderFrame
    local tabFrame = Utilities:CreateObject("Frame", {
        Parent = self._objects.baseFrame,
        BackgroundColor3 = Constants.DARK35,
        BorderSizePixel = 0,
        Size = UDim2.new(1, 0, 1, 0)
    })
    tab.tabFrame = tabFrame
    tabBtn.MouseButton1Click:Connect(function()
        self:SetActiveTab(tabName)
    end)
    self:SetActiveTab(tabName)
    local container = Container.new(tab, windowCount)
    return container
end
return Tabs
