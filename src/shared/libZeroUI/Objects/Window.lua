-- Roblox things
local RunService = game:GetService("RunService")
local LocalPlayer = game:GetService("Players").LocalPlayer

local Utilities, Constants, DraggableObjects
local Tabs
local guiParent

if RunService:IsStudio() then
    local deps = script.Parent
    guiParent = LocalPlayer.PlayerGui
    Utilities = require(deps.Parent.Utilities)
    Constants = require(deps.Parent.Constants)
    DraggableObjects = require(deps.Parent.Libraries.DraggableObjects)
    Tabs = require(deps.Tabs)
else
    guiParent = game:GetService("CoreGui")
    local require = getgenv().ZeroScripts.libZeroUI.require
    Utilities = getgenv().ZeroScripts.libZeroUI.Utilities
    Constants = getgenv().ZeroScripts.libZeroUI.Constants
    DraggableObjects = require("libzeroui/libraries/draggableobjects")
    Tabs = require("libzeroui/objects/tabs")
end

local Window = {}
Window.__index = Window

function Window.new(title, size, icon)
    local window = {
        _objects = {},
        _name = Utilities.RandomString(32),
        _tabs = {},
        _elements = {},
        _activeTab = nil,
        _title = title
    }
    local gui = Utilities:CreateObject("ScreenGui", {
        Name = window._name
    })
    window._objects.gui = gui
    -- TODO: Synapse protect gui function.
    gui.Parent = guiParent
    local windowFrame = Utilities:CreateObject("Frame", {
        Parent = gui,
        BackgroundColor3 = Constants.DARK50,
        BorderSizePixel = 0,
        Position = UDim2.new(0.5, -(size.x/2), 1, 0),
        Size = UDim2.new(0, size.x, 0, size.y)
    })
    window._objects.window = windowFrame
    window._objects.drag = DraggableObjects.new(windowFrame)
    local titleFrame = Utilities:CreateObject("Frame", {
        Parent = windowFrame,
        BackgroundColor3 = Constants.DARK60,
        BorderSizePixel = 0,
        Size = UDim2.new(1, 0, 0, 30)
    })
    local titleText = Utilities:CreateObject("TextLabel", {
        Parent = titleFrame,
        BackgroundTransparency = 1,
        Size = UDim2.new(1, 0, 1, 0),
        Font = Enum.Font.SourceSans,
        Text = title,
        TextSize = Constants.TITLE_FONT_SIZE,
        TextColor3 = Constants.WHITE,
        TextXAlignment = Enum.TextXAlignment.Center
    })
    window._objects.title = titleText
    -- If user provide icon for the GUI, use it.
    if icon then
        local titleIcon = Utilities:CreateObject("ImageLabel", {
            Parent = titleFrame,
            BackgroundTransparency = 1,
            Size = UDim2.new(0, 30, 1, 0),
            Position = UDim2.new(0, 0, 0, 0),
            Image = icon
        })
        window._objects.icon = titleIcon
    end
    local exitFrame = Utilities:CreateObject("Frame", {
        Parent = titleFrame,
        BackgroundTransparency = 1,
        BorderSizePixel = 0,
        Size = UDim2.new(0, 30, 1, 0),
        Position = UDim2.new(1, -30, 0, 0)
    })
    local exitBtn = Utilities:CreateObject("TextButton", {
        Parent = exitFrame,
        BorderSizePixel = 0,
        Size = UDim2.new(0, 16, 0, 16),
        Position = UDim2.new(0.5, -8, 0.5, -8),
        Text = ""
    })
    Utilities:CreateObject("UICorner", {
        CornerRadius = UDim.new(1, 0),
        Parent = exitBtn
    })
    Utilities:CreateObject("UIGradient", {
        Color = Constants.CLOSE_BUTTON,
        Rotation = 45,
        Parent = exitBtn
    })
    local minimizeFrame = Utilities:CreateObject("Frame", {
        Parent = titleFrame,
        BackgroundTransparency = 1,
        BorderSizePixel = 0,
        Size = UDim2.new(0, 30, 1, 0),
        Position = UDim2.new(1, -60, 0, 0)
    })
    local minimizeBtn = Utilities:CreateObject("TextButton", {
        Parent = minimizeFrame,
        BorderSizePixel = 0,
        Size = UDim2.new(0, 16, 0, 16),
        Position = UDim2.new(0.5, -8, 0.5, -8),
        Text = ""
    })
    Utilities:CreateObject("UICorner", {
        CornerRadius = UDim.new(1, 0),
        Parent = minimizeBtn
    })
    Utilities:CreateObject("UIGradient", {
        Color = Constants.MIN_BUTTON,
        Rotation = 45,
        Parent = minimizeBtn
    })
    -- Events
    exitBtn.MouseButton1Click:Connect(function()
        window:CloseWindow()
    end)
    minimizeBtn.MouseButton1Click:Connect(function()
        error("Not implemented")
    end)

    local tabs = Tabs.new(windowFrame)
    window.Tabs = tabs
    -- Set metatable to Window, to make sure every function defined outside works.
    setmetatable(window, Window)
    return window
end
-- Aliases
function Window:CreateTab(tabName, windowCount)
    return self.Tabs:CreateTab(tabName, windowCount)
end
-- Getters
function Window:GetTitle()
    return self._title
end
function Window:GetIconUrl()
    if not self._objects.icon then
        return nil
    else
        return self._objects.icon.Image
    end
end
-- Setters
function Window:SetTitle(title)
    self._title = title
    self._objects.title.Text = title
end
function Window:SetIconUrl(url)
    xpcall(function()
        if not self._objects.icon then
            self._objects.icon = Utilities:CreateObject("ImageLabel", {
                Parent = self._objects.title.Parent,
                BackgroundTransparency = 1,
                Size = UDim2.new(0, 30, 1, 0),
                Position = UDim2.new(0, 0, 0, 0),
                Image = url
            })
        else
            self._objects.icon.Image = url
        end
    end, function(_)
        error("Couldn't set the window '" .. self.name .. "' icon to " .. url)
    end)
end
-- UI related functions
function Window:HideIcon()
    local icon = self._objects.icon
    if icon then
        icon.Visible = false
    end
end
function Window:ShowIcon()
    local icon = self._objects.icon
    if icon then
        icon.Visible = true
    end
end
function Window:ShowWindow(no_wait)
    local window = self._objects.window
    window:TweenPosition(
        UDim2.new(window.Position.X.Scale, window.Position.X.Offset, 0.5, -(window.Size.Y.Offset/2)),
        Enum.EasingDirection.Out,
        Enum.EasingStyle.Quint,
        1.8,
        true,
        function()
            self.visible = true
            self._objects.drag:Enable()
        end
    )
    if not no_wait then
        wait(2) -- Make this wait
    end
end
function Window:HideWindow(no_wait)
    local window = self._objects.window
    self._objects.drag:Disable()
    window:TweenPosition(
        UDim2.new(window.Position.X.Scale, window.Position.X.Offset, 1.25, 0),
        Enum.EasingDirection.In,
        Enum.EasingStyle.Sine,
        0.8,
        true,
        function()
            self.visible = false
        end
    )
    if not no_wait then
        wait(1)
    end
end
function Window:CloseWindow()
    if self.visible then
        self:HideWindow()
    end
    self._objects.gui:Destroy()
end
return Window
