local Constants = {
    -- Colors
    DARK20 = Color3.fromRGB(20, 20, 20),
    DARK30 = Color3.fromRGB(30, 30, 30),
    DARK35 = Color3.fromRGB(35, 35, 35),
    DARK40 = Color3.fromRGB(40, 40, 40),
    DARK50 = Color3.fromRGB(50, 50, 50),
    DARK60 = Color3.fromRGB(60, 60, 60),
    GRAY100 = Color3.fromRGB(100, 100, 100),
    WHITE = Color3.fromRGB(255, 255, 255),
    BLUE = Color3.fromRGB(10, 96, 255),
    CLOSE_BUTTON = ColorSequence.new{ColorSequenceKeypoint.new(0.00, Color3.fromRGB(255, 0, 4)), ColorSequenceKeypoint.new(0.7, Color3.fromRGB(236, 26, 184)), ColorSequenceKeypoint.new(1.00, Color3.fromRGB(1, 47, 255))},
    MIN_BUTTON = ColorSequence.new{ColorSequenceKeypoint.new(0.00, Color3.fromRGB(184, 200, 0)), ColorSequenceKeypoint.new(0.55, Color3.fromRGB(11, 142, 33)), ColorSequenceKeypoint.new(1.00, Color3.fromRGB(36, 103, 31))},
    -- Fonts
    TITLE_FONT_SIZE = 18,
    FONT_SIZE = 16
}
return Constants