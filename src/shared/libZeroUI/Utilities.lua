local Utilities = {
    SetProperties = function(object, properties)
        for k, v in pairs(properties) do
            object[k] = v
        end
    end,
    RandomString = function(len)
        local str = ""
        for _ = 1, len do
            str = str .. string.char(math.random(97, 97 + 25))
        end
        return str
    end
}

function Utilities:CreateObject(objType, properties, overrides)
    local obj = Instance.new(objType)
    self.SetProperties(obj, properties)
    if overrides then
        self.SetProperties(obj, overrides)
    end
    return obj
end

return Utilities