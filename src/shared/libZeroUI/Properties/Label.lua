local RunService = game:GetService("RunService")

local Constants

if RunService:IsStudio() then
    Constants = require(script.Parent.Parent.Constants)
else
    Constants = getgenv().ZeroScripts.libZeroUI.Constants or getgenv().ZeroScripts.libZeroUI.require("libzeroui/constants")
end

return {
    BackgroundTransparency = 1,
    BorderSizePixel = 0,
    TextColor3 = Constants.WHITE,
    TextSize = 16,
    Size = UDim2.new(1, -20, 0, 20),
    Font = Enum.Font.Ubuntu,
    TextXAlignment = Enum.TextXAlignment.Left,
    TextYAlignment = Enum.TextYAlignment.Center,
}