local RunService = game:GetService("RunService")

local Constants

if RunService:IsStudio() then
    Constants = require(script.Parent.Parent.Constants)
else
    Constants = getgenv().ZeroScripts.libZeroUI.Constants or getgenv().ZeroScripts.libZeroUI.require("libzeroui/constants")
end

return {
    BackgroundColor3 = Color3.fromRGB(25, 25, 25),
    BorderColor3 = Color3.fromRGB(90, 90, 90),
    TextColor3 = Constants.WHITE,
    TextSize = 16,
    Size = UDim2.new(1, -20, 0, 25),
    Font = Enum.Font.Ubuntu
}