local RunService = game:GetService("RunService")

local Constants

if RunService:IsStudio() then
    Constants = require(script.Parent.Parent.Constants)
else
    Constants = getgenv().ZeroScripts.libZeroUI.Constants or getgenv().ZeroScripts.libZeroUI.require("libzeroui/constants")
end

return {
    Frame = {
        BackgroundColor3 = Color3.fromRGB(25, 25, 25),
        BorderColor3 = Color3.fromRGB(90, 90, 90),
        Size = UDim2.new(1, -20, 0, 25)
    },
    TextBox = {
        BackgroundTransparency = 1,
        TextColor3 = Constants.WHITE,
        TextSize = 16,
        Position = UDim2.new(0, 5, 0, 0),
        Size = UDim2.new(1, -10, 1, 0),
        Font = Enum.Font.Ubuntu,
        TextXAlignment = Enum.TextXAlignment.Left,
        TextYAlignment = Enum.TextYAlignment.Center,
    }
}