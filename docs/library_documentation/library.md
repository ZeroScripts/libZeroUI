# class `library`

## Overview

This class is a wrapper for [`Window.new`](./window.md#function-windownewtitle-string-size-x-number-y-number-icon-string) function, and provides some necessary things for the library to work.

### function library.CreateWindow(title: string, size: {x: number = 960, y: number = 540}, icon: string)

#### return [`Window`](./window.md)

This function is a shorthand for `Window.new`, because when you load the library this class gets loaded first.
