# class `Window`

## Overview

This class is responsible for Window management.

**Note:** Do not construct this class manually, instead use [`Window.new`](./window.md#function-windownewtitle-string-size-x-number-y-number-icon-string) function

## Reference

### function `Window.new(title: string, size: {x: number, y: number}, icon: string)`

#### return [`Window`](./window.md)

Create a new Window object, return a new [`Window`](./window.md) class

When you create a new `Window` using `Window.new`, this function create a private table then use `Window` as the metatable for that table, so all functions below will not work if you try to use it without using the `Window` class from `Window.new` function

### function `Window:GetTitle()`

#### return `string`

Get the current window title, will raise an error if the window is not made by `Window.new`

### function `Window:GetIconUrl()`

#### return `string`

Get the current window icon Url, will raise an error if the window is not made by `Window.new`

### function `Window:SetTitle(title: string)`

#### return `nil`

Set the title of the window, will raise an error if the window is not made by `Window.new`

### function `Window:SetIconUrl(url: string)`

#### return `nil`

Set the icon Url of the window, will raise an error if the window is not made by `Window.new`

### function `Window:HideIcon()`

#### return `nil`

Hide the window icon if the icon is available, else do nothing.

### function `Window:ShowIcon()`

#### return `nil`

Show the window icon if the icon is available, else do nothing.

### function `Window:ShowWindow(no_wait: bool = false)`

#### return `nil`

Show the window, if `no_wait` is true then it will not wait until the window has been shown on the screen.

### function `Window:HideWindow(no_wait: bool = false)`

#### return `nil`

Hide the window, if `no_wait` is true then it will not wait until the window has been hidden on the screen.

### function `Window:CloseWindow()`

#### return `nil`

Hide the window, then destroy the window object.

### class `Window.Tabs`

#### return [`Tabs`](./tabs.md)

Referer to [`Tabs`](./tabs.md) class

### function `Window:CreateTab(tabName: string, windowCount: number = 1)`

#### return [`Tabs`](./tabs.md)

A shorthand for `Window.Tabs:CreateTab(tabName, windowCount)
